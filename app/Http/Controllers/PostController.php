<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $posts = Post::paginate(5);
        return view("posts.index", compact("posts"));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view("posts.create");
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        try {
            $image = $request->file('image');
            $imageName = null;

            if ($image && $image->isValid()) {
                $path = "images/blog";
                $extension = $image->getClientOriginalExtension();
                do {
                    $imageName = Str::random(10) . '.' . $extension;
                } while (file_exists($path . '/' . $imageName));
                $image->move($path, $imageName);
            }

            Post::create([
                "title" => $request->title,
                "description" => $request->description,
                "image" => $imageName // Store the image name
            ]);

            return redirect()->route("post.index")->with("success", "The new post has been successfully added!");
        } catch (\Exception $e) {
            echo ($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        return view("posts.show", compact("post"));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        return view("posts.edit", compact("post"));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        try {
            $post = Post::findOrFail($id);
            $image = $request->file('image');
            $imageName = $post->image;

            if ($image && $image->isValid()) {
                $path = "images/blog";
                $extension = $image->getClientOriginalExtension();
                do {
                    $imageName = Str::random(10) . '.' . $extension;
                } while (file_exists($path . '/' . $imageName));
                $image->move($path, $imageName);

                // Delete the old image if a new one is uploaded
                if ($post->image && file_exists($path . '/' . $post->image)) {
                    unlink($path . '/' . $post->image);
                }
            }

            $post->update([
                "title" => $request->title,
                "description" => $request->description,
                "image" => $imageName // Update the image name
            ]);

            return redirect()->route("post.index")->with("success", "The post has been successfully updated!");
        } catch (\Exception $e) {
            echo ($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        try {
            $post = Post::findOrFail($id);
            $path = "images/blog";

            // Delete the image if it exists
            if ($post->image && file_exists($path . '/' . $post->image)) {
                unlink($path . '/' . $post->image);
            }

            $post->delete();

            return redirect()->route("post.index")->with("success", "The post has been successfully deleted!");
        } catch (\Exception $e) {
            echo ($e->getMessage());
        }
    }
}
