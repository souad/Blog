@extends('Layouts.header')

@section('content')
    <div class="min-h-screen flex justify-center items-center bg-gray-100">
        <div class="max-w-md w-full bg-white p-8 rounded-lg shadow-md">
            <h2 class="text-xl font-semibold mb-4">{{ $post->title }}</h2>

            <div class="card-body">
                <p class="mb-4">{{ $post->description }}</p>
                @if ($post->image)
                    <img src="{{ asset('storage/' . $post->image) }}" alt="{{ $post->title }}" class="w-full rounded-lg">
                @endif
            </div>
        </div>
    </div>
@endsection
