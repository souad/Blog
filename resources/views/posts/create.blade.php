@extends('layouts.header')

@section('content')

@section("titre", "Ajouter un nouveau post")
@section("contenu")
    <h1 class="mt-2 mb-2">Créer un nouveau post</h1>   

    {{-- Display success message if post creation was successful --}}
    @if(session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif

    {{-- Display error messages if there are any --}}
    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li> {{$error}} </li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="{{ route('posts.store') }}" enctype="multipart/form-data">
                @csrf
        <h1>hello soade</h1>
    </form>
@endsection
