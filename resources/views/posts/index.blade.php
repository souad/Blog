@extends("Layouts.header")
@section("title", "Liste des articles de blog")

@section("content")
    <div class="container mx-auto px-4 py-8">
        <h2 class="text-2xl font-semibold mb-4">Liste des articles de blog</h2>

        @if(session()->has("success"))
            <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative mb-4" role="alert">
                {{ session("success") }}
            </div>
        @endif

        <a href="{{ route('posts.create') }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mb-4 inline-block">Nouvel Post</a>

        @isset($posts)
            <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6">
                @foreach($posts as $post)
                    <div class="bg-white shadow-md rounded-lg overflow-hidden">
                        @if($post->image)
                            <img src="{{ asset('images/blog/' . $post->image) }}" class="w-full h-48 object-cover" alt="{{ $post->title }}" />
                        @else
                            <img src="{{ asset('images/blog/noimage.jpg') }}" class="w-full h-48 object-cover" alt="no-image" />
                        @endif
                        <div class="p-4">
                            <h5 class="text-lg font-semibold mb-2">{{ $post->title }}</h5>
                            <p class="text-gray-700 text-sm mb-4">{{ \Illuminate\Support\Str::limit($post->description, 100) }}</p>
                            <div class="flex space-x-2">
                                <a href="{{ route('posts.show', $post->id) }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-2 rounded text-sm"><i class="bi bi-eye"></i> Voir</a>
                                <a href="{{ route('posts.edit', $post->id) }}" class="bg-green-500 hover:bg-green-700 text-white font-bold py-1 px-2 rounded text-sm"><i class="bi bi-pencil"></i> Modifier</a>
                                <form action="{{ route('posts.destroy', $post->id) }}" method="post" class="inline">
                                    @csrf
                                    @method("DELETE")
                                    <button type="submit" class="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 rounded text-sm" onclick="return confirm('Voulez-vous supprimer l\'article : {{ $post->title }}?')">
                                        <i class="bi bi-trash"></i> Supprimer
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="mt-6">
                {{ $posts->links('pagination::tailwind') }}
            </div>
        @endisset
    </div>
@endsection
